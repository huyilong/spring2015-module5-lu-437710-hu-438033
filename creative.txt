1.Names and Student IDs of all the group members
Jing Lu 437710
Yilong Hu 438033

2.A link to your running ec2 instance
http://ec2-54-68-242-124.us-west-2.compute.amazonaws.com/calendar/calendar.html

You should sign up as a new user and login to use your calendar.
Or you can use default user(username:wustl, password:wustl)


3.A brief description of what you did for your creative portion
As for the creative portion of our group work, we added the following features:

(1) Users can tag an event with a particular category and enable/disable those tags in the calendar view. 
Description: We have implemented the feature that when user created a new event on the calendar, the category that the used choosed would be reflected as a colorful bullet point on the calendar. More specifically, red is for entertainment, blue is for business, yellow is for meeting, black is for sports and green is for other. In this way, user could conveniently have a sense of what kind of the events he or she would have on a certain day without clicking on the event button to see the details. Also, user could click the "tag" button to the right of the calendar to enable/disable these colorful bullet points. In the view of events of a day, events are listed in categories.



(2) Users can share their calendar with additional users
Description: In addition, user could share his or her own calendar with the events on it to friends via clicking on the "share calendar" button next to the log out button. When clicked, the website would show a list of other users for the current user to choose and share. The recipient could log into their own account to see the corresponding change and update on their calendars.



(3) Users can create group events that display on multiple users calendars
Description: We have also implemented the feature that when user clicks the "create event" button on the right, he or she would see a list of other users so that the user could create the event simultaneously for multiple users in the online calendar. To select and create events for multiple friends in the list, just simple press "command" button on your keyboard while clicking and selecting their names.


(4) Boostrap
Description: frontend improved with Bootstrap

(5) Important events list
Description: In the sidebar to the right of the screen, user can see a list of important today's events. This makes it easier for user to track their arrangements.


Notes:
(1) Sometimes it may take some time to see events display on the calendar. You can click on "today" button to get a response if you have not see events displayed
(2) Instead of showing abbreviation of event title on the calendar, we decide to use a button because if there too many events on the same day, all the abbreviations of event titles will mess up the cell on the calendar. You can click on event button on some day to display events on that days.


Robustness Design
(1)Sign-up: filter input and check if the username/password/name typed by user is valid or legal; no duplicate username is allowed
(2)Safe from XSS attacks
(3)Safe from SQL injection
(4)CSRF tokens are used
(5)HTTP only session cookie
